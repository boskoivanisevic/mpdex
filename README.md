# Mpdex

Elixir client for Music Player Daemon.

Library does not implement complete
[MPD protocol](https://mpd.readthedocs.io/en/latest/protocol.html "MPD protocol documentation").
Only essential parts for manipulating playback, music database, play
lists and play queue are implemented.

Through `Elixir.Registry` Mpdex allows client process to subscribe to
notifications and receive MPD status and play queue changes.

## Installation

Package can be installed by adding `mpdex` to your list of
dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:mpdex, "~> 1.0.0"}
  ]
end
```
## Usage

For details see `Mpdex` documentation.

### Manipulating play queue

```elixir
{:ok, mpd} = Mpdex.start_link(host: "localhost", port: 6600)

# Clear the play queue
Mpdex.clear(mpd)

# Add stream to queue
Mpdex.add_to_queue(mpd, "https://naxidigital-cafe128ssl.streaming.rs:8022#Naxi Cafe Radio")

# Add file to queue
Mpdex.add_to_queue(mpd, "Klasika/radetzky-march.mp3")
```

Mpdex will broadcast each queue change in the following format (with
all unrecognized metadata, if any, returned as a list):

```elixir
[
  %{
    file: "https://naxidigital-cafe128ssl.streaming.rs:8022#Naxi Cafe Radio",
    metadata: %{
      album: "Unknown",
      artist: "Unknown",
      id: "10",
      name: "NAXI CAFE RADIO (NAXI,Belgrade,Serbia, NAXI,Beograd,Srbija) - 128k",
      position: 0,
      time: 0,
      title: "https://naxidigital-cafe128ssl.streaming.rs:8022#Naxi Cafe Radio",
      undefined: ""
    }
  },
  %{
    file: "Klasika/radetzky-march.mp3",
    metadata: %{
      album: "The Wedding Collection 1",
      artist: "Various",
      duration: 178.04,
      genre: "Blues",
      id: "11",
      last_modified: ~U[2012-01-29 12:56:50Z],
      position: 1,
      time: 178,
      title: "Radetzky March op. 228/Johann",
      undefined: ["OK", ""]
    }
  }
]
```

### Playback

```elixir
Mpdex.play(mpd, :position, 0)
Mpdex.stop(mpd)
```

Mpdex broadcasts status as a hash of values:

```elixir
  %{
    audio: [samplerate: "44100", bits: "24", channels: "2"],
    bitrate: "128",
    consume: "0",
    elapsed: "0.878",
    mixrampdb: "0.000000",
    nextsong: "1",
    nextsongid: "11",
    playlist: "20",
    playlistlength: "2",
    random: "0",
    repeat: "0",
    single: "0",
    song: "0",
    songid: "10",
    state: "play",
    time: "1:0",
    volume: "0"
  }

```
