defmodule Mpdex.PlaylistsTest do
  use ExUnit.Case
  doctest Mpdex.Playlists

  test "gets the list of play lists" do
    {:ok, modified, _} = DateTime.from_iso8601("2018-11-24T18:40:08Z")

    expected =
      {:ok,
       [
         %{
           file: "Classic",
           metadata: %{
             title: "Classic",
             album: "",
             last_modified: modified,
             duration: nil
           },
           type: :playlist
         }
       ]}

    assert Mpdex.Playlists.list() == expected
  end

  test "gets songs from the single list" do
    expected =
      {:ok,
       [
         %{
           metadata: %{
             album: "Seascapes",
             artist: "Jon Doe",
             disc: "1/1",
             genre: ["Romantic", "Classical", "Classical", "Orchestral"],
             last_modified: ~U[2017-05-08 16:17:30Z],
             name: "Jeux de vagues",
             time: 418,
             title: "(La Mer) - II. Jeux de vagues. Allegro",
             track: "2/9",
             undefined: [
               "Label: eClassical",
               "Label: BIS",
               "Conductor: Lan Shui",
               "Date: 2007",
               "Composer: Debussy, Claude"
             ],
             duration: 418.429
           },
           type: :file,
           file: "Classic/Debussy-jeux-de-vagues.mp3"
         },
         %{
           file: "https://streaming.karolina.rs/karolina.mp3#Karolina",
           metadata: %{album: "Unknown", artist: "Uknown", time: 0, title: "Karolina"},
           type: :file
         },
         %{
           file: "https://naxidigital-cafe128ssl.streaming.rs:8022/;stream.nsv#Naxi Cafe",
           metadata: %{album: "Unknown", artist: "Uknown", time: 0, title: "Naxi Cafe"},
           type: :file
         },
         %{
           file: "https://strm112.1.fm/back280s_mobile_mp3#A list 80s - 1.fm",
           metadata: %{album: "Unknown", artist: "Uknown", time: 0, title: "A list 80s - 1.fm"},
           type: :file
         },
         %{
           file: "https://icecast.omroep.nl/radio2-bb-mp3#NPO Radio 2",
           metadata: %{album: "Unknown", artist: "Uknown", time: 0, title: "NPO Radio 2"},
           type: :file
         },
         %{
           file: "Klasika/100% Classics/01. Puccini Nessun Dorma From Turandot.mp3",
           metadata: %{
             album: "100% Classics",
             artist: "Klasika",
             duration: 181.028,
             last_modified: ~U[2000-09-25 10:28:00Z],
             time: 181,
             title: "01. Puccini Nessun Dorma From Turandot"
           },
           type: :file
         },
         %{
           file: "Klasika/._Debussy-jeux-de-vagues-24.flac",
           metadata: %{
             album: "Unknown",
             artist: "Klasika",
             last_modified: ~U[2017-07-02 17:50:05Z],
             time: 0,
             title: "._Debussy-jeux-de-vagues-24"
           },
           type: :file
         },
         %{
           file: "Klasika/Debussy-jeux-de-vagues_16.flac",
           metadata: %{
             album: "Seascapes",
             artist: "Klasika",
             disc: "1",
             duration: 418.4,
             genre: ["(32)Orchestral", "(32)Romantic"],
             last_modified: ~U[2017-05-08 19:17:54Z],
             time: 418,
             title: "(La Mer) - II. Jeux de vagues. Allegro",
             track: "2",
             undefined: ["Composer: Debussy, Claude", "Conductor: Lan Shui"]
           },
           type: :file
         }
       ]}

    assert Mpdex.Playlists.get("dummy") == expected
  end

  test "sends command to load list" do
    assert Mpdex.Playlists.load("dummy") == {:ok, "OK\n"}
  end

  test "sends command to add url to list" do
    assert Mpdex.Playlists.add_to_list("dummy", "http://example.com") == {:ok, "OK\n"}
  end

  test "sends command to clear list" do
    assert Mpdex.Playlists.clear("dummy") == {:ok, "OK\n"}
  end

  test "sends command to delete song from list" do
    assert Mpdex.Playlists.delete_song_at("dummy", 1) == {:ok, "OK\n"}
  end

  test "sends command to move song" do
    assert Mpdex.Playlists.move_song("dummy", 2, 1) == {:ok, "OK\n"}
  end

  test "sends command to save queue to list" do
    assert Mpdex.Playlists.save_queue_to_list("dummy_1") == {:ok, "OK\n"}
  end

  test "sends command to rename list" do
    assert Mpdex.Playlists.rename("dummy", "old-dummy") == {:ok, "OK\n"}
  end

  test "sends command to delete list" do
    assert Mpdex.Playlists.delete("dummy") == {:ok, "OK\n"}
  end
end
