defmodule Mpdex.TestClient do
  @behaviour Mpdex.Client

  @playlists Path.expand("../fixtures/playlists", __DIR__)
  @music_db Path.expand("../fixtures/music_db", __DIR__)

  @playlists_pattern ~r/(listplaylists|listplaylistinfo|load|playlistadd|playlistclear|playlistdelete|playlistmove|save|rename|rm)/
  @music_db_pattern ~r/(listfiles|update)/

  @impl Mpdex.Client
  def send(cmd) do
    cond do
      Regex.match?(@playlists_pattern, cmd) ->
        playlist_cmd(cmd)

      Regex.match?(@music_db_pattern, cmd) ->
        music_db_cmd(cmd)

      true ->
        {:error, :unrecognized_command}
    end
  end

  defp playlist_cmd(cmd) do
    cond do
      cmd == "listplaylists" ->
        File.read(Path.join(@playlists, "list.txt"))

      cmd == "listplaylistinfo \"dummy\"" ->
        File.read(Path.join(@playlists, "get.txt"))

      cmd == "load \"dummy\"" ->
        {:ok, "OK\n"}

      cmd == "playlistadd \"dummy\" \"http://example.com\"" ->
        {:ok, "OK\n"}

      cmd == "playlistclear \"dummy\"" ->
        {:ok, "OK\n"}

      cmd == "playlistdelete \"dummy\" 1" ->
        {:ok, "OK\n"}

      cmd == "playlistmove \"dummy\" 2 1" ->
        {:ok, "OK\n"}

      cmd == "save \"dummy_1\"" ->
        {:ok, "OK\n"}

      cmd == "rename \"dummy\" \"old-dummy\"" ->
        {:ok, "OK\n"}

      cmd == "rm \"dummy\"" ->
        {:ok, "OK\n"}

      true ->
        {:error, :unknown_playlist_command}
    end
  end

  defp music_db_cmd(cmd) do
    cond do
      "listfiles \"\"" == cmd ->
        File.read(Path.join(@music_db, "list.txt"))

      "update \"\"" == cmd ->
        {:ok, "updating_db: 1\nOK\n"}

      true ->
        {:error, :unknown_music_db_command}
    end
  end
end
