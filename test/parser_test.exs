defmodule Mpdex.ParserTest do
  use ExUnit.Case, async: true

  test "parsing list of playlists" do
    response = """
    playlist: Pop 80s
    Last-Modified: 2023-03-06T20:20:38Z
    playlist: Radio
    Last-Modified: 2023-03-06T19:36:15Z
    OK
    """

    expected =
      {:ok,
       [
         %{
           file: "Pop 80s",
           metadata: %{
             title: "Pop 80s",
             album: "",
             last_modified: ~U[2023-03-06 20:20:38Z],
             duration: nil
           },
           type: :playlist
         },
         %{
           file: "Radio",
           metadata: %{
             title: "Radio",
             album: "",
             last_modified: ~U[2023-03-06 19:36:15Z],
             duration: nil
           },
           type: :playlist
         }
       ]}

    assert Mpdex.Parser.parse_list_of_playlists(response) == expected
  end

  test "parsing queue" do
    response = """
    file: https://strm112.1.fm/back280s_mobile_mp3#A list 80s - 1.fm#A list 80s - 1.fm
    Pos: 0
    Id: 3
    file: Klasika/100% Classics/01. Puccini Nessun Dorma From Turandot.mp3
    Last-Modified: 2000-09-25T10:28:00Z
    Time: 181
    duration: 181.028
    Pos: 1
    Id: 4
    OK
    """

    expected =
      {:ok,
       [
         %{
           file: "https://strm112.1.fm/back280s_mobile_mp3#A list 80s - 1.fm#A list 80s - 1.fm",
           metadata: %{
             album: "Unknown",
             artist: "Uknown",
             id: "3",
             position: 0,
             time: 0,
             title: "A list 80s - 1.fm#A list 80s - 1.fm"
           },
           type: :file
         },
         %{
           file: "Klasika/100% Classics/01. Puccini Nessun Dorma From Turandot.mp3",
           metadata: %{
             album: "100% Classics",
             artist: "Klasika",
             duration: 181.028,
             id: "4",
             last_modified: ~U[2000-09-25 10:28:00Z],
             position: 1,
             time: 181,
             title: "01. Puccini Nessun Dorma From Turandot"
           },
           type: :file
         }
       ]}

    assert Mpdex.Parser.parse_playlist(response) == expected
  end

  test "parsing playlist content" do
    response = """
    file: 80s/Amii Stewart - Knock on wood.mp3
    Last-Modified: 2020-01-01T17:28:55Z
    Time: 210
    duration: 209.928
    file: 80s/Baccara - Yes sir, I can boogie.mp3
    Last-Modified: 2020-01-01T17:27:37Z
    Time: 272
    duration: 271.560
    OK
    """

    expected =
      {:ok,
       [
         %{
           file: "80s/Amii Stewart - Knock on wood.mp3",
           metadata: %{
             album: "Unknown",
             artist: "80s",
             duration: 209.928,
             last_modified: ~U[2020-01-01 17:28:55Z],
             time: 210,
             title: "Amii Stewart - Knock on wood"
           },
           type: :file
         },
         %{
           file: "80s/Baccara - Yes sir, I can boogie.mp3",
           metadata: %{
             album: "Unknown",
             artist: "80s",
             duration: 271.56,
             last_modified: ~U[2020-01-01 17:27:37Z],
             time: 272,
             title: "Baccara - Yes sir, I can boogie"
           },
           type: :file
         }
       ]}

    assert Mpdex.Parser.parse_playlist(response) == expected
  end

  test "parsling list of files" do
    response = """
    directory: Flac
    Last-Modified: 2018-11-28T19:59:44Z
    directory: Classic
    Last-Modified: 2017-05-03T20:19:09Z
    directory: 80s
    Last-Modified: 2021-07-22T05:52:49Z
    directory: Bossa Nova and Soul
    Last-Modified: 2020-07-23T13:27:41Z
    """

    expected =
      {:ok,
       [
         %{
           file: "Flac",
           metadata: %{
             album: "Unknown",
             artist: "Unknown",
             last_modified: ~U[2018-11-28 19:59:44Z],
             time: 0,
             title: "Flac"
           },
           type: :directory
         },
         %{
           file: "Classic",
           metadata: %{
             album: "Unknown",
             artist: "Unknown",
             last_modified: ~U[2017-05-03 20:19:09Z],
             time: 0,
             title: "Classic"
           },
           type: :directory
         },
         %{
           file: "80s",
           metadata: %{
             album: "Unknown",
             artist: "Unknown",
             last_modified: ~U[2021-07-22 05:52:49Z],
             time: 0,
             title: "80s"
           },
           type: :directory
         },
         %{
           file: "Bossa Nova and Soul",
           metadata: %{
             album: "Unknown",
             artist: "Unknown",
             last_modified: ~U[2020-07-23 13:27:41Z],
             time: 0,
             title: "Bossa Nova and Soul"
           },
           type: :directory
         }
       ]}

    assert Mpdex.Parser.parse_listfiles(response) == expected
  end
end
