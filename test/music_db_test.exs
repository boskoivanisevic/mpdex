defmodule Mpdex.MusicDbTest do
  use ExUnit.Case
  doctest Mpdex.MusicDb

  test "gets the list" do
    res = {
      :ok,
      [
        %{
          file: "Classical Top 100 - Volume 6",
          type: :directory,
          metadata: %{
            album: "Unknown",
            artist: "Unknown",
            time: 0,
            last_modified: ~U[2013-03-03 13:31:31Z],
            title: "Classical Top 100 - Volume 6"
          }
        },
        %{
          file: "radetzky-march.mp3",
          type: :file,
          metadata: %{
            album: "Unknown",
            artist: "Unknown",
            time: 0,
            last_modified: ~U[2012-01-29T12:56:50Z],
            size: 2_848_655,
            title: "radetzky-march"
          }
        },
        %{
          file: "100% Classics",
          type: :directory,
          metadata: %{
            album: "Unknown",
            artist: "Unknown",
            time: 0,
            last_modified: ~U[2002-06-06 13:39:57Z],
            title: "100% Classics"
          }
        },
        %{
          file: "Russian Folk Songs - Moscow Nights.mp3",
          type: :file,
          metadata: %{
            album: "Unknown",
            artist: "Unknown",
            time: 0,
            last_modified: ~U[2012-01-29 12:56:54Z],
            size: 2_893_824,
            title: "Russian Folk Songs - Moscow Nights"
          }
        }
      ]
    }

    assert Mpdex.MusicDb.list() == res
  end

  test "updates database" do
    assert Mpdex.MusicDb.update() == :ok
  end
end
