defmodule Mpdex.MixProject do
  use Mix.Project

  @version "1.0.3"

  def project do
    [
      app: :mpdex,
      version: @version,
      elixir: "~> 1.13",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      docs: docs(),
      description: description(),
      package: package(),
      source_url: "https://gitlab.com/boskoivanisevic/mpdex"
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {Mpdex.Application, []},
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [{:ex_doc, "~> 0.27", only: :dev, runtime: false}]
  end

  defp docs do
    [
      authors: ["Bosko Ivanišević"],
      source_ref: "v#{@version}",
      extras: ["README.md"],
      main: "readme"
    ]
  end

  defp description do
    """
    Elixir client for Music Player Daemon.
    """
  end

  defp package do
    [
      files: ~w(lib .formatter.exs mix.exs README* LICENSE*),
      maintainers: ["Boško Ivanišević"],
      licenses: ["MIT"],
      links: %{
        "Gitllab" => "https://gitlab.com/boskoivanisevic/mpdex",
        "MPD protocol" => "https://mpd.readthedocs.io/en/latest/protocol.html"
      }
    ]
  end
end
