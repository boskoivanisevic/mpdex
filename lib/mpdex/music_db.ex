defmodule Mpdex.MusicDb do
  @moduledoc false

  def list(uri \\ "") do
    case client().send("listfiles \"#{uri}\"") do
      {:ok, raw_content} ->
        Mpdex.Parser.parse_listfiles(raw_content)

      {:error, %{"err_msg" => error}} ->
        {:error, error}

      {:error, nil} ->
        {:error, []}
    end
  end

  def lsinfo(uri \\ "") do
    case client().send("lsinfo \"#{uri}\"") do
      {:ok, raw_content} ->
        Mpdex.Parser.parse_listfiles(raw_content)

      {:error, %{"err_msg" => error}} ->
        {:error, error}

      {:error, nil} ->
        {:error, []}
    end
  end

  def update(uri \\ "") do
    case client().send("update \"#{uri}\"") do
      {:ok, _} -> :ok
      _ -> :error
    end
  end

  defp client do
    Application.get_env(:mpdex, :mpd_client, Mpdex.SocketClient)
  end
end
