defmodule Mpdex.Playback do
  @moduledoc false

  def crossfade(seconds) when is_integer(seconds) do
    client().send("crossfade #{seconds}")
  end

  def play(what, pos) do
    case {what, pos} do
      {:position, position} ->
        client().send("play #{position}")

      {:id, id} ->
        client().send("playid #{id}")

      _ ->
        {:error, "Invalid options"}
    end
  end

  def next(), do: client().send("next")

  def resume(), do: client().send("pause 0")

  def pause(), do: client().send("pause 1")

  def previous, do: client().send("previous")

  def random_off, do: client().send("random 0")
  def random_on, do: client().send("random 1")

  def repeat_off, do: client().send("repeat 0")
  def repeat_on, do: client().send("repeat 1")

  def single_off, do: client().send("single 0")
  def single_on, do: client().send("single 1")

  def seek(time) when is_float(time), do: client().send("seekcur #{time}")

  def forward(time) when is_float(time), do: client().send("seekcur +#{time}")

  def backward(time) when is_float(time), do: client().send("seekcur -#{time}")

  def stop(), do: client().send("stop")

  def volume(vol) when is_integer(vol) and vol >= 0 and vol <= 100 do
    client().send("setvol #{vol}")
  end

  defp client() do
    Application.get_env(:mpdex, :mpd_client, Mpdex.SocketClient)
  end
end
