defmodule Mpdex.Client do
  @moduledoc false

  @callback send(cmd :: binary) :: {:ok, :any} | {:error, atom() | binary() | map()}
end
