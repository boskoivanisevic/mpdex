defmodule Mpdex.Status do
  @moduledoc false

  def idle(subsystem) do
    client().send("idle #{subsystem}")
  end

  @spec status() :: map()
  def status(), do: send_cmd("status")

  def statistics(), do: send_cmd("stats")

  def current_song(), do: send_cmd("currentsong")

  defp client() do
    Application.get_env(:mpdex, :mpd_client, Mpdex.SocketClient)
  end

  defp send_cmd(cmd) do
    case client().send(cmd) do
      {:ok, response} ->
        Mpdex.Parser.parse_key_value(response)

      _ ->
        %{}
    end
  end
end
