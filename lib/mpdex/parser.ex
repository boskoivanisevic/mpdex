defmodule Mpdex.Parser do
  @moduledoc false

  @float_values ~w(duration mixramdelay elapsed)
  @integer_values ~w(repeat random single consume playlistlength song bitrate volume xfade uptime playtime artists albums songs db_playtime mixrampdb)
  @unix_time_values ~w(db_update)

  def parse_list_of_playlists(raw_lists) do
    playlists =
      raw_lists
      |> String.split("\n")
      |> Enum.chunk_every(2)
      |> Enum.reduce([], fn entry, acc ->
        case entry do
          [<<"playlist: ", name::binary>>, <<"Last-Modified: ", modified::binary>>] ->
            modified =
              case DateTime.from_iso8601(modified) do
                {:ok, date_time, _} ->
                  date_time

                _ ->
                  nil
              end

            playlist = %{
              file: name,
              metadata: %{
                title: name,
                last_modified: modified,
                album: "",
                duration: nil
              },
              type: :playlist
            }

            [playlist | acc]

          _ ->
            acc
        end
      end)
      |> Enum.reverse()

    {:ok, playlists}
  end

  def parse_playlist(raw_list) do
    [_ | raw_songs] = String.split(raw_list, "file: ")

    songs =
      raw_songs
      |> Enum.reduce([], fn line, acc ->
        {file, metadata} = parse_entry_line(line)

        [%{file: file, metadata: metadata, type: :file} | acc]
      end)
      |> Enum.reverse()

    {:ok, songs}
  end

  def parse_listfiles(raw_content) do
    regex = ~r/(directory: |file: |playlist: )/

    data =
      Regex.split(regex, raw_content, include_captures: true, trim: true)
      |> Enum.chunk_every(2)

    content =
      for [entry_type, entry_line] <- data, reduce: [] do
        acc ->
          {file, metadata} = parse_entry_line(entry_line)

          type =
            cond do
              String.starts_with?(entry_type, "file") -> :file
              String.starts_with?(entry_type, "directory") -> :directory
              String.starts_with?(entry_type, "playlist") -> :playlist
            end

          [%{type: type, file: file, metadata: metadata} | acc]
      end
      |> Enum.reverse()

    {:ok, content}
  end

  defp parse_entry_line(line) do
    [file | raw_metadata] =
      String.split(line, "\n", trim: true)
      |> Enum.filter(fn val -> "OK" != val end)

    metadata =
      parse_metadata(raw_metadata)
      |> Enum.reduce(%{}, fn {key, val}, acc ->
        case acc do
          %{^key => existing} when is_binary(existing) ->
            Map.put(acc, key, [existing, val])

          %{^key => existing} when is_list(existing) ->
            Map.put(acc, key, [val | existing])

          _ ->
            Map.put(acc, key, val)
        end
      end)
      |> fill_missing_metadata(file)

    {file, metadata}
  end

  defp parse_metadata(metadata) do
    Enum.map(metadata, fn md ->
      case md do
        <<"Artist: ", artist::binary>> ->
          {:artist, artist}

        <<"Album: ", album::binary>> ->
          {:album, album}

        <<"Disc: ", disc::binary>> ->
          {:disc, disc}

        <<"duration: ", raw_duration::binary>> ->
          duration =
            if String.contains?(raw_duration, ".") do
              raw_duration
            else
              "#{raw_duration}.0"
            end
            |> String.to_float()

          {:duration, duration}

        <<"Genre: ", genre::binary>> ->
          {:genre, genre}

        <<"Id: ", id::binary>> ->
          {:id, id}

        <<"Last-Modified: ", last_modified::binary>> ->
          modified =
            case DateTime.from_iso8601(last_modified) do
              {:ok, modified_time, _} ->
                modified_time

              {:error, _} ->
                nil
            end

          {:last_modified, modified}

        <<"Name: ", name::binary>> ->
          {:name, name}

        <<"Pos: ", position::binary>> ->
          {:position, String.to_integer(position)}

        <<"Time: ", time::binary>> ->
          {:time, String.to_integer(time)}

        <<"Title: ", title::binary>> ->
          {:title, title}

        <<"Track: ", track::binary>> ->
          {:track, track}

        <<"size: ", size::binary>> ->
          {:size, String.to_integer(size)}

        rest ->
          {:undefined, rest}
      end
    end)
  end

  def parse_key_value(raw_value) do
    raw_value
    |> String.split("\n")
    |> Enum.map(fn val -> convert_value(String.split(val, ": ")) end)
    |> Enum.reject(&is_nil(&1))
    |> Enum.into(%{})
  end

  def convert_value([mpd_name, value]) when mpd_name in @integer_values,
    do: {mpd_name |> String.downcase() |> String.to_atom(), String.to_integer(value)}

  def convert_value([mpd_name, value]) when mpd_name in @float_values,
    do: {mpd_name |> String.downcase() |> String.to_atom(), String.to_float(value)}

  def convert_value([mpd_name, value]) when mpd_name in @unix_time_values,
    do:
      {mpd_name |> String.downcase() |> String.to_atom(),
       DateTime.from_unix!(String.to_integer(value))}

  def convert_value(["audio", value]) do
    [sample, bits, channels] = String.split(value, ":")

    {:audio,
     [
       {:samplerate, String.to_integer(sample)},
       {:bits, String.to_integer(bits)},
       {:channels, String.to_integer(channels)}
     ]}
  end

  def convert_value([mpd_name, value]),
    do: {mpd_name |> String.downcase() |> String.to_atom(), value}

  def convert_value(_), do: nil

  defp fill_missing_metadata(metadata, file) do
    [artist, album, title] = extract_from_file_path(file)

    metadata =
      if false == Map.has_key?(metadata, :artist) do
        Map.put(metadata, :artist, artist)
      else
        metadata
      end

    metadata =
      if false == Map.has_key?(metadata, :album) do
        Map.put(metadata, :album, album)
      else
        metadata
      end

    metadata =
      if false == Map.has_key?(metadata, :title) do
        Map.put(metadata, :title, title)
      else
        metadata
      end

    if false == Map.has_key?(metadata, :time) do
      Map.put(metadata, :time, 0)
    else
      metadata
    end
  end

  defp extract_from_file_path(file_path) when "http" == binary_part(file_path, 0, 4) do
    uri = URI.parse(file_path)
    ["Uknown", "Unknown", uri.fragment || String.trim_leading(uri.path, "/")]
  end

  defp extract_from_file_path(file_path) do
    paths = Path.dirname(file_path) |> String.split("/")

    [artist, album] =
      case length(paths) do
        1 when ["."] != paths ->
          [hd(paths), "Unknown"]

        1 ->
          ["Unknown", "Unknown"]

        2 ->
          paths

        _ ->
          [artist | [album | _]] = paths
          [artist, album]
      end

    name = Path.basename(file_path, Path.extname(file_path))

    [artist, album, name]
  end
end
